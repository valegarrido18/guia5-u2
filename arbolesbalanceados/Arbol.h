#include <iostream>
// Definición nodo.
//~ #include "programa.h"

#ifndef ARboL_H
#define ARboL_H

typedef struct _Nodo {
    int dato;
    // Altura del subarbol derecho menos bo del subarbol izquierdo.
    int factor_equilibrio;

    struct _Nodo *izq;
    struct _Nodo *der;
    struct _Nodo *nodo_padre;
} Nodo;

class Arbol{
    private:

    public:
        Arbol();
        Nodo* crear_nodo(int numero, Nodo *padre);
        void insertar(Nodo *&raiz, int numero, bool &bo, Nodo *padre);
        void eliminar(Nodo *&raiz, bool &bo, int numero);

        void reestructuracion_izq(bool bo, Nodo *&raiz);
        // Rotaciones para la reestructuración izquierda-
        void rotacion_DD(Nodo *&raiz, bool bo, Nodo *nodo1);
        void rotacion_DI(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2);

        void reestructuracion_der(bool bo, Nodo *&raiz);
        // Rotaciones para la reestructuración derecha.
        void rotacion_II(Nodo *&raiz, bool bo, Nodo *nodo1);
        void rotacion_ID(Nodo *&raiz, Nodo *nodo1, Nodo *nodo2);
};
#endif

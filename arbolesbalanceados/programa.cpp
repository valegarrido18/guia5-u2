#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
#include "Arbol.h"

using namespace std;

/* para usar fork() */
#include <unistd.h>

class Graficar {
    private:
        Nodo *arbol = NULL;

    public:
        // Constructor Clase Graficar.
        Graficar(Nodo *raiz) {
	        this->arbol = raiz;
        }

        // ofstream tipo de dato que corresponde a archivos en cpp (el llamado es ofstream &nombre_archivo).
        void recorrerArbol(Nodo *p, ofstream &archivo) {
	        string infoTmp;
	        // Unión los nodos del grafo para diferenciar entre izq y der, se le entrega un identificador a cada nodo.
	        if (p != NULL) {
	       
	        if (p->izq != NULL) {
		        archivo<< p->dato << "->" << p->izq->dato << "[label=" << p->factor_equilibrio << "];" << endl;
	        }

	        else {
		        infoTmp = to_string(p->dato) + "i";
		        infoTmp = "\"" + infoTmp + "\"";
		        archivo << infoTmp << "[shape=point]" << endl;
		        archivo << p->dato << "->" << infoTmp << ";" << endl;
	        }
	        
	        infoTmp = p->dato;
	        
	        if (p->der != NULL) {
		        archivo << p->dato << "->" << p->der->dato << "[label=" << p->factor_equilibrio << "];" << endl;
	        }

	        else {
		        infoTmp = to_string(p->dato) + "d";
		        infoTmp = "\"" + infoTmp + "\"";
		        archivo << infoTmp << "[shape=point]" << endl;
		        archivo << p->dato << "->" << infoTmp << ";" << endl;
	        }
	        
	        // Llamados por izquierda y derecha para crear el grafo.
	        recorrerArbol(p->izq, archivo);
	        recorrerArbol(p->der, archivo);
	        }

	        return;
        }

        void crearGraficar() {
	        ofstream archivo;
	        archivo.open("datos.txt");
	        archivo << "digraph G {" << endl;
	        archivo << "node [style=filled fillcolor=pink];" << endl;
	        // Llamado a la función recursiva que efectúa el archivo de texto para crear el grafo.
	        recorrerArbol(this->arbol, archivo);
	        archivo << "}" << endl;
	        archivo.close();
	        
	        // Genéra el grafo.
	        system("dot -Tpng -ografo.png datos.txt &");
	        system("eog grafo.png &");
        }
};


int ingreso_numero(){
    int numero;

    system("clear");
    cout << "\nDigite un número: ";
    cin >> numero;

    return numero;
}

void ingresar_nodo(Arbol *nuevo_arbol, Nodo *&raiz, bool &bo){
    Nodo *nodo = NULL;
    // Pedir número del nodo.
    int numero = ingreso_numero();

    // Crear primer nodo.
    nodo = nuevo_arbol->crear_nodo(numero, NULL);

    // Se inicializa la raiz del arbol.
    if(raiz == NULL){
        raiz = nodo;
    }

    // Más de un nodo.          
    else{
        nuevo_arbol->insertar(raiz, numero, bo, NULL);
    }
}

void menu(Arbol *nuevo_arbol, Nodo *raiz){
    int opcion; 
    bool bo = false;
    int numero;
 
    do{ 

        cout << "\n  >              MENU              <\n" << endl;
        cout << "         Ingresar número  [1]" << endl;
        cout << "         Eliminar nodo    [2]" << endl;
        cout << "         Modificar nodo   [3]" << endl;
        cout << "         Mostrar grafo    [4]" << endl;
        cout << "         Salir            [5]" << endl;
     
        cout << "\n  >> Ingrese opción: ";
        cin >> opcion;
       
        switch(opcion){
            case 1:
                // Crea nodo e ingresa el dato.
                ingresar_nodo(nuevo_arbol, raiz, bo);
                break;

            case 2:
                // Eliminación del nodo para número ingresado.
                numero = ingreso_numero();
                nuevo_arbol->eliminar(raiz, bo, numero);
                break;

            case 3: 
                // Eliminación del nodo antiguo y agrega el nuevo.
                numero = ingreso_numero();
                nuevo_arbol->eliminar(raiz, bo, numero);

                cout << ">> Nuevo número: ";
                ingresar_nodo(nuevo_arbol, raiz, bo);
                break;

            case 4:
                // Muestra la grafica del árbol
                Graficar *g = new Graficar(raiz);
                g->crearGraficar();
                break;
        }
        system("clear");
    }
    while(opcion != 5);
}

int main(){
    Arbol *nuevo_arbol = new Arbol();
    Nodo *raiz = NULL;
    
    menu(nuevo_arbol, raiz);

    return 0;
}
